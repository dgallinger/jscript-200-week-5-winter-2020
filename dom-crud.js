// Create a new <a> element containing the text "Buy Now!" 
// with an id of "cta" after the last <p>

const link = document.createElement('a');
link.innerText = 'Buy Now!';
link.id = 'cta';

const main = document.querySelector('main');
main.appendChild(link);

// Access (read) the data-color attribute of the <img>,
// log to the console
const imgEl = document.getElementsByTagName('img');
//console.log(imgEl.dataset.color);

// Update the third <li> item ("Turbocharged"), 
// set the class name to "highlight"
//const listEl = document.getElementsByTagName('ul');
//const el3 = listEl.getElementsByTagName('li');
const listEl = document.getElementsByTagName('li');
listEl[2].className = 'highlight';

// Remove (delete) the last paragraph
// (starts with "Available for purchase now…")
const paraEl = document.getElementsByTagName('p');
//main.removeChild(paraEl[0]);

const lastP = paraEl[paraEl.length - 1];
main.removeChild(lastP);