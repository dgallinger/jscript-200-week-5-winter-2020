// When a user clicks the + element, the count should increase by 1 on screen.
// When a user clicks the – element, the count should decrease by 1 on screen.
let count = 0;
const counterEl = document.getElementById('count');

const plusEl = document.getElementById('plus');
const minusEl = document.getElementById('minus');

plusEl.addEventListener('click', () => {
    count++;
    counterEl.innerText = count;
});

minusEl.addEventListener('click', () => {
    count--;
    counterEl.innerText = count;
});