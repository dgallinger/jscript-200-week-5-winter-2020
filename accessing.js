// Change the text of the "Seattle Weather" header to "February 10 Weather Forecast, Seattle"
const link = document.getElementById('weather-head');

link.innerText = 'Feb 10 Weather Forecast, Seattle';

// Change the styling of every element with class "sun" to set the color to "orange"
const sunElements = document.getElementsByClassName('sun');  //this gives an array-like object. need to iterate through and change each color to orange.

//need to convert to array and change el.style.color = 'orange';
const elements = Array.from(sunElements);
elements.forEach(el => el.style.color = 'orange');

// Change the class of the second <li> from to "sun" to "cloudy"
elements[0].className = 'cloudy';