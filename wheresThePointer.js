// Attach one listener that will detect clicks on any of the <td>
// elements.  Should update that element's innerHTML to be the
// x, y coordinates of the mouse at the time of the click

//add a listener to the body
const tbody = document.querySelector('tbody');

//add event listener to the tbody
tbody.addEventListener('click', function(e){
    const td = e.target;
    
    //click point x, y
    td.innerText = (`${e.clientX}, ${e.clientY}`);
});