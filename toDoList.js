// If an li element is clicked, toggle the class "done" on the <li>

ulEl = document.querySelector('ul');

document.querySelectorAll('span').forEach(item => {
  item.addEventListener('click', event => {
    //handle click
    item.parentNode.className = 'done';
  })
});

// If a delete link is clicked, delete the li element / remove from the DOM

document.querySelectorAll('.delete').forEach(item => {
  item.addEventListener('click', event => {
    //handle click
    ulEl.removeChild(item.parentNode);
  })
});

// If an 'Add' link is clicked, adds the item as a new list item with
// addListItem function has been started to help you get going!
// Make sure to add an event listener(s) to your new <li> (if needed)

const addListItem = function(e) {
  //e.preventDefault();
  // const input = this.parentNode.getElementsByTagName('input')[0];
  // const text = input.value; // use this text to create a new <li>

  //get the text from the document
  const input = document.querySelectorAll('input')[0];
  const text = input.value;

  //create the span element
  const textSpan = document.createElement('span');
  textSpan.innerText = (`${text} `);

  //create the delete element
  const deleteA = document.createElement('a');
  deleteA.innerText = 'Delete';
  deleteA.className = 'delete';

  //create the list element and append the span and a elements
  const link = document.createElement('li');
  link.appendChild(textSpan);
  link.appendChild(deleteA);
  
  //add the list element to document
  ulEl.appendChild(link);

  //add event listeners
  textSpan.addEventListener('click', event => {
    link.className = 'done';
  });

  deleteA.addEventListener('click', event => {
    ulEl.removeChild(link);
  });

  //clear the input value
  input.value = '';

};

const addEl = document.querySelector('.add-item');

addEl.addEventListener('click', event => {
  addListItem()
});