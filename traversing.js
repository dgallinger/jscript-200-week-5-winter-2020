// Given the <body> element as variable body,
// access the <main> node and log to the console.
const body = document.querySelector('body');

const main = body.getElementsByTagName('main');

console.log(main.body);

// Given the <ul> element as variable ul,
// access <body>  and log to the console.
const ul = document.querySelector('ul');
const body1 = ul.parentNode.parentNode;

console.log(body1);

// Given the <p> element as var p,
// access the 3rd <li>  and log to the console.
const p = document.querySelector('p');

const li3 = p.previousSibling.previousSibling.children[2];
console.log(li3);
